<?php
  class Controller_Specialty extends Controller
  {

  	 function __construct()
	{
		$this->model = new Model_Specialty();
		$this->view = new View();
	}
    function action_index()
	{	     

		$data = $this->model->get_data();		
		$this->view->generate('specialty_view.php', 'template_view.php', $data);
	}
  }