<?php
class Controller_Work extends Controller
{
    function __construct()
	{
		$this->model = new Model_Work();
		$this->view = new View();
	}
    /*function action_index()
	{ 

		
		$this->view->generate('work_view.php', 'template_view.php', $data);
	}*/
	function action_work($id)
	{ 
	
		$data = $this->model->get_data_par($id);		
		$this->view->generate('work_view.php', 'template_view.php', $data);
	}
}